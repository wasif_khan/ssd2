import comparingboxes as cm
import numpy as np
import random

c = cm.comparing()

all_anchor_boxes = c.get_all_anchor_boxes()
# # np.save('all_anchor_boxes.npy', all_anchor_boxes)
# all_anchor_boxes = np.load('all_anchor_boxes.npy', allow_pickle = True)
my_box = [110, 120, 170, 270]
my_boxes = []

for i in range(10):
    y1 = random.randint(1, 101)
    x1 = random.randint(1, 101)
    y2 = random.randint(y1, 300)
    x2 = random.randint(x1, 300)
    my_boxes.append([y1, x1, y2, x2])

for my_box in my_boxes:

    print('------------------------------------------')
    print('Given Box: ', my_box)
    ######################## Aspect Ratio #######################################
    ar = c.calculate_aspect_ratio(my_box)
    closest_ar = c.compute_closest_aspect_ratio(ar)
    print('my box Aspect Ratio: ', ar)
    print('closesest Anchor Box aspect ratio: ', closest_ar)

    ####################### Scale ###############################################

    scale = c.compute_scale_of_box(all_anchor_boxes, my_box)
    print('Scale: ', scale)

    ############################################# Eucledian Distance ########################
    min_distance = c.calculate_eucledian_distance(my_box, all_anchor_boxes, scale['scale_index'])
    print('Eucledian Distance: ', min_distance)
    min_distance_index = min_distance['index']

    min_distance_whole = c.calculate_eucledian_distance_for_whole_box(my_box, all_anchor_boxes)
    print('Eucledian Distance Whole: ', min_distance_whole)
    scale_by_distance = min_distance_whole['scale']

    index_aspect_ratio = c.compute_aspect_ratio_index(scale['scale_index'], closest_ar)
    print('Index of Aspect Ratio: ', c.compute_aspect_ratio_index(scale['scale_index'], closest_ar))


    print('Closest Anchor Box: ', np.round(all_anchor_boxes[scale['scale_index']][min_distance_index[0]][min_distance_index[1]][index_aspect_ratio]))

    print('Closest Anchor Box By Eucledian Distance Whole: ', np.round(all_anchor_boxes[scale_by_distance][min_distance_whole['index'][0]][min_distance_whole['index'][1]][min_distance_whole['index'][2]]))
