from nets import ssd_vgg_300
import numpy as np

class comparing:

    def __init__(self):
        self.all_possible_aspect_ratios = np.array([1., 0.5, 1./3, 2, 3])
        self.aspect_ratio_4_boxes = [1, 1, 0.5, 2]
        self.aspect_ratio_6_boxes = [1, 1, 0.5, 2, 1./3, 3]

    def compute_aspect_ratio_index(self, scale, aspect_ratio):
        if scale == 0 or scale == 4 or scale == 5:
            return self.aspect_ratio_4_boxes.index(aspect_ratio)
        else:
            return self.aspect_ratio_6_boxes.index(aspect_ratio)
            

    def bb_intersection_over_union(self, boxA, boxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])
    
        # compute the area of intersection rectangle
        interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    
        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
        boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    
        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)
    
        # return the intersection over union value
        return iou

    def calculate_eucledian_distance(self, my_box, bboxes, scale):
        img_size = 300
        min_values = []
        min_values_indexes = []
        distance_and_index = {}
        my_box_center = [0, 0]
        my_box_center[0] = round((my_box[2] + my_box[0])/2)
        my_box_center[1] = round((my_box[3] + my_box[1])/2)

        # for i in range(len(bboxes)):

        centers_of_bboxes = np.zeros([bboxes[scale].shape[0], bboxes[scale].shape[1], bboxes[scale].shape[2], 2])
        centers_of_bboxes[:, :, :, 0] = np.round((bboxes[scale][:, :, :, 2] +  bboxes[scale][:, :, :, 0]) / 2)
        centers_of_bboxes[:, :, :, 1] = np.round((bboxes[scale][:, :, :, 3] +  bboxes[scale][:, :, :, 1]) / 2)
        distances = np.zeros([bboxes[scale].shape[0], bboxes[scale].shape[1], bboxes[scale].shape[2]])

        distances = np.sqrt(np.square( centers_of_bboxes[:, :, :, 0] - my_box_center[0] ) + np.square( centers_of_bboxes[:, :, :, 1] - my_box_center[1] )) 

            # distances = np.sqrt(np.square(bboxes[i][:, :, :, 0] - my_box[0]) + np.square(bboxes[i][:, :, :, 1] - my_box[1]) + np.square(bboxes[i][:, :, :, 2] - my_box[2]) + np.square(bboxes[i][:, :, :, 3] - my_box[3]))
        # min_values.append(np.min(distances))
        # min_values_indexes.append(np.unravel_index(np.argmin(distances), distances.shape))

        distance_and_index['distance'] = round(np.argmin(distances), 2)
        # distance_and_index['distance'] = round(min_values[np.argmin(min_values)], 2)
        # distance_and_index['index'] = min_values_indexes[np.argmin(min_values)]
        distance_and_index['index'] = np.unravel_index(np.argmin(distances), distances.shape)
        # distance_and_index['scale'] = np.argmin(min_values)
        return distance_and_index

    def calculate_eucledian_distance_for_whole_box(self, my_box, bboxes):
        img_size = 300
        min_values = []
        min_values_indexes = []
        distance_and_index = {}
        my_box_center = [0, 0]
        my_box_center[0] = round((my_box[2] + my_box[0])/2)
        my_box_center[1] = round((my_box[3] + my_box[1])/2)

        for i in range(len(bboxes)):

            distances = np.zeros([bboxes[i].shape[0], bboxes[i].shape[1], bboxes[i].shape[2]])
            distances = np.sqrt(np.square(bboxes[i][:, :, :, 0] - my_box[0]) + np.square(bboxes[i][:, :, :, 1] - my_box[1]) + np.square(bboxes[i][:, :, :, 2] - my_box[2]) + np.square(bboxes[i][:, :, :, 3] - my_box[3]))
            min_values.append(np.min(distances))
            min_values_indexes.append(np.unravel_index(np.argmin(distances), distances.shape))

        distance_and_index['distance'] = round(min_values[np.argmin(min_values)], 2)
        distance_and_index['index'] = min_values_indexes[np.argmin(min_values)]
        distance_and_index['scale'] = np.argmin(min_values)
        return distance_and_index

    def get_all_anchor_boxes(self):
        feat_shapes=[(38, 38), (19, 19), (10, 10), (5, 5), (3, 3), (1, 1)]
        anchor_sizes=[(21., 45.),
                        (45., 99.),
                        (99., 153.),
                        (153., 207.),
                        (207., 261.),
                        (261., 315.)]
        anchor_ratios=[[2, .5], [2, .5, 3, 1./3], [2, .5, 3, 1./3], [2, .5, 3, 1./3], [2, .5], [2, .5]]
        anchor_steps=[8, 16, 32, 64, 100, 300]
        anchor_offset=0.5
        normalizations=[20, -1, -1, -1, -1, -1]
        prior_scaling=[0.1, 0.1, 0.2, 0.2]
        no_of_boxes_per_location_for_scale = [4, 6, 6, 6, 4, 4]
        img_size = 300
        bboxes_all_scales = []
        for i in range(6):

            y, x, h, w = ssd_vgg_300.ssd_anchor_one_layer(img_shape = (300, 300),
                                feat_shape = feat_shapes[i],
                                sizes = anchor_sizes[i],
                                ratios = anchor_ratios[i],
                                step = anchor_steps[i],
                                offset=0.5);
            h = h.reshape(1, no_of_boxes_per_location_for_scale[i])
            w = w.reshape(1, no_of_boxes_per_location_for_scale[i])
            bboxes = np.zeros([y.shape[0], y.shape[1], h.shape[1], 4])
            bboxes[:, :, :, 0] = img_size * (y - h / 2.)
            bboxes[:, :, :, 1] = img_size *(x - w / 2.)
            bboxes[:, :, :, 2] = img_size *(y + h / 2.)
            bboxes[:, :, :, 3] = img_size *(x + w / 2.)
            bboxes_all_scales.append(bboxes)
        return bboxes_all_scales

    def calculate_aspect_ratio(self, my_box):
        h = my_box[2] - my_box[0]
        w = my_box[3] - my_box[1]
        return round(h/w, 2)

    def compute_closest_aspect_ratio(self, aspect_ratio_of_my_box):
        
        closest_ = np.sqrt(np.square(self.all_possible_aspect_ratios - aspect_ratio_of_my_box))
        return self.all_possible_aspect_ratios[np.argmin(closest_)]

    def compute_scale_of_box(self, all_anchor_boxes, my_box):

        jaccard_index_array = []
        for i in range(len(all_anchor_boxes)):
            jaccard_index_array.append(np.zeros([all_anchor_boxes[i].shape[0], all_anchor_boxes[i].shape[1], all_anchor_boxes[i].shape[2]]))

        for i in range(len(all_anchor_boxes)):
            for j in range(len(all_anchor_boxes[i])):
                for k in range(len(all_anchor_boxes[i][j])):
                    for l in range(len(all_anchor_boxes[i][j][k])):
                        jaccard_index_array[i][j][k][l] = self.bb_intersection_over_union(all_anchor_boxes[i][j][k][l], my_box)
        max_values = []
        max_values_index = []
        shape_and_index = {}
        for i in range(len(all_anchor_boxes)):
            # If you want to get the index of maximum value of jaccard index
            max_values_index.append(np.unravel_index(np.argmax(jaccard_index_array[i]), jaccard_index_array[i].shape))

            max_values.append(np.max(jaccard_index_array[i]))
        shape_and_index['shape'] = all_anchor_boxes[np.argmax(max_values)].shape
        shape_and_index['matrix_index'] = max_values_index[np.argmax(max_values)]
        shape_and_index['scale_index'] = np.argmax(max_values)

        return shape_and_index